document.addEventListener('DOMContentLoaded', (ev)=>{
    let form = document.getElementById('camera-form');

    let input = document.getElementById('capture');
    
    input.addEventListener('change', (ev)=>{
        console.dir( input.files[0] );
        if(input.files[0].type.indexOf("image/") > -1){
            let img = document.getElementById('img');
            img.height = 250;
            img.width = 250;

            img.src = window.URL.createObjectURL(input.files[0]);    
        }

        else if(input.files[0].type.indexOf("audio/") > -1 ){
            let audio = document.getElementById('audio');

            audio.src = window.URL.createObjectURL(input.files[0]);
        }

        else if(input.files[0].type.indexOf("video/") > -1 ){
            let video = document.getElementById('video');

            video.height = 250;
            video.width = 250;

            video.src=window.URL.createObjectURL(input.files[0]);
        }      
    })
    
})