console.log('Hello World');

const form = document.querySelector('form');
const responseElements = document.querySelector('.responses');
const connectionInfo = document.querySelector('.connection-info');
const API_URL = 'http://localhost:5000/adress';

var responseArray = [];

listAllResponses();
averageResponseTime();

form.addEventListener('submit', (event) => {
    event.preventDefault();
    const formData = new FormData(form);
    const IpAdress = formData.get('IPAdress');

    const formResult = {
        IpAdress
    };

    fetch(API_URL, {
        method: 'POST',
        body: JSON.stringify(formResult),
        headers: {
            'content-type': 'application/json'
        }
    }).then(response => response.json())
      .then(sentToClient => {
          console.log(sentToClient);
      });

    averageResponseTime(IpAdress);
});

function listAllResponses() {
    fetch(API_URL)
        .then(response => response.json())
        .then(responseTimes => {
            console.log(responseTimes);
            responseTimes.forEach(responseTime => {
                const div = document.createElement('div');

                const header = document.createElement('h3');
                header.textContent = responseTime.IpAdress;

                const result = document.createElement('p');
                result.textContent = responseTime.responseTime;

                div.appendChild(header);
                div.appendChild(result);

                responseElements.appendChild(div);
            })
        });
}

function averageResponseTime(IpAdress) {
    fetch(API_URL)
        .then(response => response.json())
        .then(responseTimes => {
            responseTimes.forEach(responseTime => {
                responseArray.push(responseTime.responseTime);
            })
            
        });
    const div = document.createElement('div');

    const header = document.createElement('h3');
    header.textContent = IpAdress;

    const averageResponse = document.createElement('p');
    const average = responseArray => responseArray.reduce((a,b) => a + b, 0) / responseArray.length;
    averageResponse.textContent = average(responseArray);
    console.log(responseArray)

    div.appendChild(header);
    div.appendChild(averageResponse);

    connectionInfo.appendChild(div);
}