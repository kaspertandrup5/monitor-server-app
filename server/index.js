const express = require('express');
const cors = require('cors');
const ping = require('net-ping');
const monk = require('monk');

const app = express();
const session = ping.createSession();

const db = monk('localhost/responceTime');
const adress = db.get('adress');

var target = '';

app.use(cors());
app.use(express.json());

app.get('/', (req, res) => {
    res.json({
        message: 'Hello'
    });
});

app.get('/adress', (req, res) =>{
    adress
        .find()
        .then(adress => {
            res.json(adress);
        });
});

app.post('/adress', (req, res) => {
    target = req.body.IpAdress.toString();
    pingIP(res);
});

pingIP = function(res) {
    session.pingHost(target, function(error, target, sent, rcvd){
        var ms = rcvd - sent;
        if (error)
            console.log(target + ': ' + error.toString());
        else {
            console.log (target + ": Alive (ms=" + ms + ")");
            const pingResult = {
                IpAdress: target,
                responseTime: ms
            }

            adress
                .insert(pingResult)
                .then(sentToClient => {
                    res.json(sentToClient);
                });
        }
    });
}

const interval = setInterval(function() {
    pingIP();
}, 5000);

app.listen(5000, () => {
    console.log('listening on port 5000');
});