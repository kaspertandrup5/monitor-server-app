const staticCacheName = 'site-static';
const assets = [
    '/',
    '/index.html',
    '/landingpage.html',
    '/pages/responsetime.html',
    '/client/js/app.js',
    '/client/js/ui.js',
    '/client/js/vendors/materialize.min.js',
    '/client/css/styles.css',
    '/client/css/vendors/materialize.min.css',
    'https://fonts.googleapis.com/icon?family=Material+Icons',
    'https://fonts.gstatic.com/s/materialicons/v47/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2',
    'https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css'
];

self.addEventListener('install', evt => {
    console.log('Service Worker has been installed!');
    evt.waitUntil(
        caches.open(staticCacheName).then(cache => {
            console.log('caching...');
            cache.addAll(assets);
        })
    )
});

self.addEventListener('activate', evt => {
    console.log('Service Worker has been activated!');
});

self.addEventListener('fetch', evt => {
    evt.respondWith(
        caches.match(evt.request).then(cacheRes => {
            return cacheRes || fetch(evt.request);
        })
    )
});